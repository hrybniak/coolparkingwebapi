﻿using CoolParking.BL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.DTO
{
    public class VehicleDTO
    {
        public string Id { get; set; }
        public decimal Balance { get; set; }
        public int VehicleType { get; set; }
    }
}
