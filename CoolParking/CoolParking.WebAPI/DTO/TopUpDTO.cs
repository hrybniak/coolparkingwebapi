﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.DTO
{
    public class TopUpDTO
    {
        public string Id { get; set; }
        public decimal Sum { get; set; }
    }
}
