﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using CoolParking.WebAPI.DTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        private readonly IParkingService _parkingService;

        public TransactionsController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        [HttpGet("last")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult GetLastTransaction()
        {
            var transaction = _parkingService.GetLastParkingTransactions();
            return Ok(transaction);
        }

        [HttpGet("all")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult GetAllTransactions()
        {
            var transactions = _parkingService.ReadFromLog();
            return Ok(transactions);
        }

        [HttpPut("topUpVehicle")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult TopUpVehicle([FromBody] TopUpDTO topUp)
        {
            try
            {
                Regex regex = new Regex("^[A-Z]{2}-[0-9]{4}-[A-Z]{2}$");
                if (regex.IsMatch(topUp.Id) && topUp.Sum>0)
                {
                    _parkingService.TopUpVehicle(topUp.Id, topUp.Sum);
                    var vehicle = _parkingService.GetVehicles().ToList().Find(v => v.Id == topUp.Id);
                    if (vehicle == null) throw new ArgumentNullException();
                    else return Ok(vehicle);
                }
                else
                {
                    return BadRequest(new { message = "Id is invalid" });
                }
            }
            catch(ArgumentException ex)
            {
                return NotFound(ex.Message);
            }
        }
    }
}