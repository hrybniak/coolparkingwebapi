﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.WebAPI.DTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        private readonly IParkingService _parkingService;

        public VehiclesController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult GetVehicles()
        {
            var vehicles = _parkingService.GetVehicles();
            return Ok(vehicles);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult GetVehicle(string id)
        {
            Regex regex = new Regex("^[A-Z]{2}-[0-9]{4}-[A-Z]{2}$");
            if (regex.IsMatch(id))
            {
                var vehicle = _parkingService.GetVehicles().ToList().Find(v => v.Id == id);
                if (vehicle == null) return NotFound(new { message = "Couldn't find vehicle with this Id" });
                else return Ok(vehicle);
            }
            else return BadRequest(new { message = "Id is invalid" });
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult AddVehicle([FromBody] VehicleDTO vehicle)
        {
            try
            {
                if (vehicle.VehicleType >= 0 && vehicle.VehicleType < 4)
                {
                    _parkingService.AddVehicle(new Vehicle(vehicle.Id, (VehicleType)vehicle.VehicleType, vehicle.Balance));
                    var vehicleCreated = _parkingService.GetVehicles().ToList().Find(v => v.Id == vehicle.Id);
                    if (vehicleCreated == null) throw new ArgumentNullException();
                    else return CreatedAtAction(nameof(GetVehicle), new { id = vehicleCreated.Id }, vehicleCreated);
                }
                else return BadRequest(new { message = "VehicleType is invalid"});
            }
            catch(ArgumentException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
            catch(InvalidOperationException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult RemoveVehicle(string id)
        {
            try
            {
                Regex regex = new Regex("^[A-Z]{2}-[0-9]{4}-[A-Z]{2}$");
                if (regex.IsMatch(id))
                {
                    _parkingService.RemoveVehicle(id);
                    return NoContent();
                }
                else return BadRequest(new { message = "Id is invalid." });
            }
            catch(ArgumentException ex)
            {
                return NotFound(new { message = ex.Message });
            }
            catch(InvalidOperationException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }
    }
}