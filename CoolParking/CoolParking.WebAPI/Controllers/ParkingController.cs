﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParkingController : ControllerBase
    {
        private readonly IParkingService _parkingService;

        public ParkingController(IParkingService parkingService)
        {
            _parkingService = parkingService;
        }

        [HttpGet("balance")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult GetBalance()
        {
            decimal balance = _parkingService.GetBalance();
            return Ok(balance);
        }

        [HttpGet("capacity")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult GetCapacity()
        {
            int capacity = _parkingService.GetCapacity();
            return Ok(capacity);
        }

        [HttpGet("freePlaces")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public IActionResult GetFreePlaces()
        {
            int freePlaces = _parkingService.GetFreePlaces();
            return Ok(freePlaces);
        }
    }
}