﻿using CoolParking.BL.Models;
using CoolParking.BL.Services;
using System;
using System.Collections.Generic;
using System.Net.Http;
using Newtonsoft.Json;
using System.Linq.Expressions;
using System.Text;

namespace CoolParkingClient
{
    public static class Menu
    {
        public static void RunMenu()
        {
            try
            {
                var httpClient = new HttpClient();
                string baseUrl = "https://localhost:44335/api/";
                uint key;
                do
                {
                    Console.WriteLine("Welcome to the parking, to choose one of the options, please press key from the menu below");
                    Console.WriteLine("Press 1 to check parking balance");
                    Console.WriteLine("Press 2 to see the amount of money parking has earned for last minute");
                    Console.WriteLine("Press 3 to check number of available lots at the parking");
                    Console.WriteLine("Press 4 to see all last minute transactions");
                    Console.WriteLine("Press 5 to see all transactions");
                    Console.WriteLine("Press 6 to add transport to the parking");
                    Console.WriteLine("Press 7 to remove transport from the parking");
                    Console.WriteLine("Press 8 to see balance of certain transport");
                    Console.WriteLine("Press 9 to replanish balance of certain transport");
                    Console.WriteLine("Press 0 to exit program");
                    Console.WriteLine("Enter key: ");
                    key = Convert.ToUInt32(Console.ReadLine());

                    switch (key)
                    {
                        case 1:
                            var requestBalance = httpClient.GetAsync(baseUrl + "parking/balance").Result;
                            if (requestBalance.IsSuccessStatusCode)
                            {
                                var balanceString = requestBalance.Content.ReadAsStringAsync().Result;
                                Console.WriteLine($"A parking has balance of {Convert.ToDecimal(balanceString):C}");
                            }
                            else throw new HttpRequestException();
                            break;
                        case 2:
                            var requestLastTransactions = httpClient.GetAsync(baseUrl + "transactions/last").Result;
                            if (requestLastTransactions.IsSuccessStatusCode)
                            {
                                var transactionsString = requestLastTransactions.Content.ReadAsStringAsync().Result;
                                var lastTransactions = JsonConvert.DeserializeObject<IEnumerable<TransactionInfo>>(transactionsString);
                                decimal sum = 0.0M;
                                foreach (var transaction in lastTransactions)
                                {
                                    sum += transaction.Sum;
                                }
                                Console.WriteLine($"A parking has earned {sum:C}");
                            }
                            else throw new HttpRequestException();
                            break;
                        case 3:
                            var availableRequest = httpClient.GetAsync((baseUrl + "parking/freePlaces")).Result;
                            var capacityRequest = httpClient.GetAsync((baseUrl + "parking/capacity")).Result;
                            if (availableRequest.IsSuccessStatusCode && capacityRequest.IsSuccessStatusCode)
                            {
                                var available = availableRequest.Content.ReadAsStringAsync().Result;
                                var capacity = capacityRequest.Content.ReadAsStringAsync().Result;
                                Console.WriteLine($"A parking has {available} available lots" +
                                    $"out of {capacity} lots");
                            }
                            else throw new HttpRequestException();
                            break;
                        case 4:
                            requestLastTransactions = httpClient.GetAsync(baseUrl + "transactions/last").Result;
                            if (requestLastTransactions.IsSuccessStatusCode)
                            {
                                Console.WriteLine("Transaction Time------------TransportId----------------Transaction Total");
                                var transactionsString = requestLastTransactions.Content
                                    .ReadAsStringAsync().Result;
                                var lastTransactions = JsonConvert.DeserializeObject<IEnumerable<TransactionInfo>>(transactionsString);
                                foreach (TransactionInfo tr in lastTransactions)
                                {
                                    Console.WriteLine($"{tr.TransactionTime}----------{tr.TransportID}-----------{tr.Sum:C}");
                                }
                            }
                            else throw new HttpRequestException();
                            break;
                        case 5:
                            var requestLog = httpClient.GetAsync(baseUrl + "transactions/all").Result;
                            if (requestLog.IsSuccessStatusCode)
                            {
                                Console.WriteLine($"All transactions: ");
                                var log = requestLog.Content.ReadAsStringAsync().Result;
                                Console.Write(log);
                            }
                            else throw new HttpRequestException();
                            break;

                        case 6:
                            Console.WriteLine("Please, enter your transport type. Available options are: car, truck, motorcycle, bus");
                            string type = Console.ReadLine();
                            Console.WriteLine("Please, enter your transport registration plate number");
                            string plateNumber = Console.ReadLine();
                            Console.WriteLine("Please, enter you transport balance");
                            decimal balance = Convert.ToDecimal(Console.ReadLine());
                            VehicleType vehicleType;
                            switch (type)
                            {
                                case "car":
                                    vehicleType = VehicleType.PassengerCar;
                                    break;
                                case "truck":
                                    vehicleType = VehicleType.Truck;
                                    break;
                                case "bus":
                                    vehicleType = VehicleType.Bus;
                                    break;
                                case "motorcycle":
                                    vehicleType = VehicleType.Motorcycle;
                                    break;
                                default:
                                    throw new ArgumentException("Invalid transport type");
                            }
                            
                            var request = httpClient.PostAsync(baseUrl + "vehicles",
                                new StringContent(
                                    JsonConvert.SerializeObject(new Vehicle(plateNumber, vehicleType, balance)),Encoding.UTF8, "application/json")).Result;
                            if (!request.IsSuccessStatusCode) throw new HttpRequestException();
                            break;
                        case 7:
                            Console.WriteLine("Please, enter transport ID of a transport your want to remove.");
                            string idToRemove = Console.ReadLine();
                            var requestDelete = httpClient.DeleteAsync(baseUrl + "vehicles/" + idToRemove).Result;
                            if (!requestDelete.IsSuccessStatusCode) throw new HttpRequestException();
                            break;
                        case 8:
                            Console.WriteLine("Please, enter transport ID of a transport of which you want to check balance.");
                            string idToCheckBalance = Console.ReadLine();
                            var vehicleRequest = httpClient.GetAsync(baseUrl + "vehicles/" + idToCheckBalance).Result;
                            if (vehicleRequest.IsSuccessStatusCode)
                            {
                                var vehicleString = vehicleRequest.Content.ReadAsStringAsync().Result;
                                var vehicle = JsonConvert.DeserializeObject<Vehicle>(vehicleString);
                                Console.WriteLine("This transport has balance: " + vehicle.Balance);
                            }
                            else throw new HttpRequestException();
                            break;
                        case 9:
                            Console.WriteLine("Please, enter transport ID of a transport of which you want to replanish balance.");
                            string idToReplanishBalance = Console.ReadLine();
                            Console.WriteLine("Please, enter amount of your replanishment");
                            decimal replanishment = Convert.ToDecimal(Console.ReadLine());
                            var putRequest = httpClient.PutAsync(baseUrl + "transactions/topUpVehicle",
                                new StringContent(JsonConvert.SerializeObject(new
                                    {Id = idToReplanishBalance, Sum = replanishment}), Encoding.UTF8, "application/json")).Result;
                            if (!putRequest.IsSuccessStatusCode) throw new HttpRequestException();
                            break;
                        case 0:
                            break;
                        default:
                            Console.WriteLine("You entered invalid option, please try again, to exit press 0 after menu.");
                            break;
                    }


                } while (key != 0);

            }
            catch (Exception ex)
            {
                Console.WriteLine($"{ ex.GetType()} says { ex.Message}");
                Console.ReadLine();
                RunMenu();
            }
        }
    }
}
