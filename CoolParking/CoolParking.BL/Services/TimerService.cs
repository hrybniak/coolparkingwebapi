﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.
// Why do I need to do this? It doesn't make any sense.

using CoolParking.BL.Interfaces;
using System;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        private readonly Timer _timer;
        public TimerService()
        {
            _timer = new Timer();
            _timer.Elapsed += OnTimerElapsed;
        }
        private void OnTimerElapsed(object sender, ElapsedEventArgs elapsedEventArgs)
        {
            Elapsed?.Invoke(sender, elapsedEventArgs);
        }
        public double Interval 
            { get => _timer.Interval;
              set => _timer.Interval = value; }

        public event ElapsedEventHandler Elapsed;
        public void Start()
        {
            _timer.Start();
        }

        public void Stop()
        {
            _timer.Stop();
        }
        private bool _disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    _timer.Dispose();
                }
            }
            this._disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}