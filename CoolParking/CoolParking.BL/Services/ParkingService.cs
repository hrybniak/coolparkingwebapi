﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private readonly Parking _parking;
        private readonly ITimerService _withdrawTimer;
        private readonly ITimerService _logTimer;
        private readonly ILogService _logService;
        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            _parking = Parking.Instanse;
            _withdrawTimer = withdrawTimer;
            _logTimer = logTimer;
            _logService = logService;
            SetTransactionProcessing();
        }

        private void SetTransactionProcessing()
        {
            _withdrawTimer.Interval = Settings.billingPeriodInSeconds*1000;
            _logTimer.Interval = Settings.loggingPeriodInSeconds*1000;
            _withdrawTimer.Elapsed += DoTransactions;
            _logTimer.Elapsed += LogTransactions;
            _withdrawTimer.Start();
            _logTimer.Start();
        }
        public decimal GetAmountOfMoneyEarnedForLastPeriod()
        {
            decimal sum = 0.0M;
            foreach(TransactionInfo tr in _parking.Transactions)
            {
                sum += tr.Sum;
            }
            return sum;
        }
        public decimal GetVehicleBalance(string id)
        {
            var vehicle = _parking.Vehicles.Find(v => v.Id == id);
            if (vehicle == null) throw new ArgumentException("There is no vehicle with this plate number");
            else return vehicle.Balance;
        }
        public void AddVehicle(Vehicle vehicle)
        {
            if (GetFreePlaces() == 0) throw new InvalidOperationException("There is no free places on this parking");
            else if (_parking.Vehicles.Find(v => v.Id == vehicle.Id) != null) throw new ArgumentException("The car is already on the parking");
            else _parking.Vehicles.Add(vehicle);
        }

        public decimal GetBalance() => _parking.Balance;

        public int GetCapacity() => (int)Settings.maxCapacity;

        public int GetFreePlaces() => (int)(Settings.maxCapacity - _parking.Vehicles.Count);

        public TransactionInfo[] GetLastParkingTransactions() => _parking.Transactions.ToArray();
        public ReadOnlyCollection<Vehicle> GetVehicles() => new ReadOnlyCollection<Vehicle>(_parking.Vehicles);

        public string ReadFromLog() => _logService.Read();
        
        public void RemoveVehicle(string vehicleId)
        {
            var vehicle = _parking.Vehicles.Find(v => v.Id == vehicleId);
            if (vehicle == null) throw new ArgumentException("There is no car with this id on the parking");
            else if (vehicle.Balance < 0) throw new InvalidOperationException("Can't remove car with negative balance");
            else
            {
                _parking.Vehicles.Remove(vehicle);
            }
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (sum <= 0) throw new ArgumentException("Can't top up with negative or zero");
            else if (!_parking.Vehicles.Any(v => v.Id == vehicleId)) throw new ArgumentException("There is no transport with this id");
            else
            {
                var vehicle = _parking.Vehicles.Find(v => v.Id == vehicleId);
                vehicle.Balance += sum;
            }
        }

        private bool _disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    _withdrawTimer.Dispose();
                    _logTimer.Dispose();
                    _parking.Dispose();
                }
            }
            this._disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void DoTransactions(object sender, ElapsedEventArgs e) 
        {
            foreach(Vehicle v in _parking.Vehicles.ToArray())
            {
                decimal parkingCost = 0.0M;
                if (v.Balance < 0) 
                    parkingCost = (decimal)(Settings.penaltyRatio) * v.GetParkingCost();
                else if (v.Balance - v.GetParkingCost() < 0)
                    parkingCost = v.Balance + (v.GetParkingCost() - v.Balance) * (decimal)Settings.penaltyRatio;
                else 
                    parkingCost = v.GetParkingCost();
                v.Balance -= parkingCost;
                _parking.Balance += parkingCost;
                var transaction = new TransactionInfo(parkingCost, v.Id);
                _parking.Transactions.Add(transaction);
            }
        }
        private void LogTransactions(object sender, ElapsedEventArgs e)
        {
            if (_parking.Transactions.Count == 0) _logService.Write(""); 
            foreach(TransactionInfo tr in _parking.Transactions.ToArray())
            {
                _logService.Write($"{tr.TransactionTime}--------------{tr.TransportID}------------------{tr.Sum}");
                _parking.Transactions.Remove(tr);
            }
        }
    }
}