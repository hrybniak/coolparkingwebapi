﻿using Newtonsoft.Json;
using System;

namespace CoolParking.BL.Models
{
    public class TransactionInfo
    {
        public DateTime TransactionTime { get; private set; }
        public decimal Sum { get; private set; }
        public string TransportID { get; private set; }

        public TransactionInfo(decimal sum, string transportId)
        {
            TransactionTime = DateTime.Now;
            Sum = sum;
            TransportID = transportId;
        }

        [JsonConstructor]
        public TransactionInfo(string transportId, DateTime transactionTime, decimal sum)
        {
            TransactionTime = transactionTime;
            Sum = sum;
            TransportID = transportId;
        }
    }
}