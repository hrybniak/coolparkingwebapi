﻿using System;
using System.Collections;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public string Id { get; private set; }
        public VehicleType VehicleType { get; private set; }
        public decimal Balance { get; internal set; }

        public Vehicle(string id, VehicleType type, decimal balance)
        {
            if (ValidateId(id) && balance >= 0)
            {
                Id = id;
                VehicleType = type;
                Balance = balance;
            }
            else throw new ArgumentException("Invalid input data for new vehicle.");
        }

        public static string GenerateRandomRegistrationPlateNumber()
        {
            char[] alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();
            var rand = new Random();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < 2; i++)
            {
                sb.Append(alphabet[rand.Next(0, alphabet.Count() - 1)]);
            }
            sb.Append("-");
            for (int i = 0; i < 4; i++)
            {
                sb.Append(rand.Next(0, 9));
            }
            sb.Append("-");
            for (int i = 0; i < 2; i++)
            {
                sb.Append(alphabet[rand.Next(0, alphabet.Count() - 1)]);
            }
            return sb.ToString();
        }
        
        public decimal GetParkingCost()
        {
            switch (VehicleType)
            {
                case VehicleType.Bus:
                    return Settings.busParkingCost;
                case VehicleType.Truck:
                    return Settings.trackParkingCost;
                case VehicleType.PassengerCar:
                    return Settings.carParkingCost;
                case VehicleType.Motorcycle:
                    return Settings.motorcycleParkingCost;
                default:
                    throw new ArgumentException();
            }
        } 
        private bool ValidateId(string id)
        {
            Regex regex = new Regex("^[A-Z]{2}-[0-9]{4}-[A-Z]{2}$");
            return regex.IsMatch(id);
        }
    }
}